package main

import (
	"log"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"expense-tracker-golang/handlers"
	"expense-tracker-golang/models"
)

func init() {
	err := models.InitDB(os.Getenv("DBUSER"), os.Getenv("DBPASSWORD"), os.Getenv("DBADDR"), os.Getenv("DBNAME"))
	if err != nil {
		log.Println(err)
		log.Fatalln("Unable to connect to database")
	}

	log.Println("Success connecting to database")
}

func main() {
	lambda.Start(handler)
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	log.Println("Request payload", request)
	log.Println("HTTPMethod", request.HTTPMethod)
	log.Println("Path", request.Path)
	log.Println("QueryStringParameters", request.QueryStringParameters)
	log.Println("PathParameters", request.PathParameters)

	var response events.APIGatewayProxyResponse
	var err error

	switch request.HTTPMethod {
	case "POST":
		response, err = handlers.ProcessAddExpense(request)
	case "GET":
		response, err = handlers.ProcessListExpenses(request)
	default:
		response = handlers.ProcessUnknownRequest()
	}

	return response, err
}
