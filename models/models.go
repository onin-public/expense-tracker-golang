package models

import (
	"database/sql"
	"log"

	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

type ExpenseItem struct {
	Description string  `json:"description"`
	Price       float32 `json:"price"` // TODO: Float for now, needs to be changed later on to a reliable data type
}

func InitDB(dbUser string, dbPassword string, dbAddr string, dbName string) error {
	var err error

	cfg := mysql.Config{
		User:                 dbUser,
		Passwd:               dbPassword,
		Net:                  "tcp",
		Addr:                 dbAddr, // "<HOST>:<PORT>",
		DBName:               dbName,
		AllowNativePasswords: true,
	}

	log.Printf("Connecting to DB. dbUser %v dbAddr %v dbName %v\n", dbUser, dbAddr, dbName)
	db, err = sql.Open("mysql", cfg.FormatDSN())
	if err != nil {
		log.Fatalln(err)
		return err
	}

	return db.Ping()
}

func InsertExpense(description string, price float32) (int64, error) {
	result, err := db.Exec("INSERT INTO expenses (description, price) VALUES (?, ?)", description, price)
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return id, nil
}

func GetLatestExpenses(limit int) ([]ExpenseItem, error) {
	queryResult, err := db.Query("SELECT description, price FROM expenses ORDER BY id DESC LIMIT ?", limit)
	if err != nil {
		return nil, err
	}

	result := make([]ExpenseItem, 0)

	for queryResult.Next() {
		var expenseItem ExpenseItem
		err = queryResult.Scan(&expenseItem.Description, &expenseItem.Price)
		if err != nil {
			return nil, err
		}

		result = append(result, expenseItem)
	}

	return result, nil
}
