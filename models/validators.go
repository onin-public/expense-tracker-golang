package models

func IsValidDescrition(description string) bool {
	if len(description) <= 0 {
		return false
	}
	if len(description) > 50 {
		return false
	}

	return true
}

func IsValidPrice(price float32) bool {
	if price < 0 {
		return false
	}
	if price > 9999999 {
		return false
	}

	return true
}
