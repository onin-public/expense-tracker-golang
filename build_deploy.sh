# Build
GOOS=linux go build -o output/main
cd output
zip build.zip main

# Deploy - Command for initial upload
# aws iam create-role --role-name lambda-ex --assume-role-policy-document '{"Version": "2012-10-17", "Statement": [{ "Effect": "Allow", "Principal": { "Service": "lambda.amazonaws.com"}, "Action": "sts:AssumeRole"}]}'
# aws iam attach-role-policy --role-name lambda-ex --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
# aws lambda create-function \
#   --function-name expense-tracker-golang \
#   --zip-file fileb://build.zip \
#   --handler main \
#   --runtime go1.x \
#   --role arn:aws:iam::<accountid>:role/lambda-ex
#   --region ap-southeast-1

# Deploy - Command for updating the lambda
aws lambda update-function-code \
  --function-name expense-tracker-golang \
  --zip-file fileb://build.zip \
  --region ap-southeast-1

# Cleanup
cd ..
rm -rf output