package handlers

import (
	"encoding/json"
	"expense-tracker-golang/models"
	"fmt"
	"log"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
)

/*
Example valid request payload for POST

	{
		"description": "Burger",
		"price": 145.99
	}
*/
func ProcessAddExpense(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var item models.ExpenseItem

	err := json.Unmarshal([]byte(request.Body), &item)
	if err != nil {
		log.Println("Error parsing payload", err)
		return events.APIGatewayProxyResponse{}, err
	}

	if models.IsValidDescrition(item.Description) != true {
		return generateReponseError(400, "Description is invalid"), nil
	}
	if models.IsValidPrice(item.Price) != true {
		return generateReponseError(400, "Price is invalid"), nil
	}

	_, err = models.InsertExpense(item.Description, item.Price)
	if err != nil {
		log.Println("Error saving expense to database", err)
		return events.APIGatewayProxyResponse{}, err
	}

	message := fmt.Sprintf("Expense item '%v' worth %v was recorded. Thank you!!", item.Description, item.Price)
	response := events.APIGatewayProxyResponse{
		StatusCode: 201,
		Body:       message,
	}

	return response, nil
}

func ProcessListExpenses(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var limit int = 5
	var err error

	// TODO: Use a validator library to make this concise
	strlimit, exists := request.QueryStringParameters["limit"]
	if exists {
		limit, err = strconv.Atoi(strlimit)
		if err != nil {
			return generateReponseError(400, "Limit is not an integer"), nil
		}
		if limit < 1 {
			return generateReponseError(400, "Limit is less than 1"), nil
		}
		if limit > 10 {
			return generateReponseError(400, "Limit is more than 10"), nil
		}
	}

	// Get the 5 latest expenses
	expenses, err := models.GetLatestExpenses(limit)
	if err != nil {
		log.Println("Error retrieving data from DB", err)
		return events.APIGatewayProxyResponse{}, err
	}

	response := generateReponseJsonBody(200, expenses)

	return response, nil
}

func ProcessUnknownRequest() events.APIGatewayProxyResponse {
	response := events.APIGatewayProxyResponse{
		StatusCode: 405,
	}

	return response
}

func generateReponseJsonBody(httpStatus int, body any) events.APIGatewayProxyResponse {
	jsonBody, _ := json.Marshal(body)

	response := events.APIGatewayProxyResponse{
		StatusCode: httpStatus,
		Body:       string(jsonBody),
	}

	return response
}

func generateReponseError(httpStatus int, err string) events.APIGatewayProxyResponse {
	responseBody := make(map[string]string)
	responseBody["error"] = err

	return generateReponseJsonBody(httpStatus, responseBody)
}
